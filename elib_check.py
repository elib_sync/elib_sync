#!/usr/bin/python
# -*- coding: utf-8 -*-

from lxml import etree

import urllib2
import sys, os
import time
import isbn
import mad
#import eyeD3
#from mutagen.easyid3 import EasyID3
from mutagen.mp3 import MP3
from mutagen.id3 import ID3, TALB, TPE2, TPE1, COMM, TCOM, TIT1, TPOS, TCON, TLAN, TIT2, TRCK, TYER, TDAT, TPUB, TKEY, POPM
import elib_sync
import elib_conf

URL = 'http://www.elib.se/library/ebook_detail.asp?id_type=ISBN&id=%(isbn)s&lib=%(library)s'


def get_book_info(isbn10):
    resp = urllib2.urlopen(URL % {'library': elib_conf.LIBRARY, 'isbn': isbn10})
    html = resp.read()
    html = html.decode('iso-8859-1')
    tree = etree.HTML(html)
    
    book_info = tree.xpath('/html/body/table/tr/td[2]/table[2]/tr/td[3]/table/tr')
    #print len(book_info)
    author = book_info[0].xpath('td/span/text()')[1:]
    others = author[1:]
    author = author[0]
    title = book_info[1].xpath('td[2]/span/text()')[0]
    bookformat, length = book_info[2].xpath('td[2]/span/text()')[1].strip()[:-1].split('(')
    description = ''.join(book_info[4].xpath('td[2]/span[2]/text()'))
    category = book_info[5].xpath('td[2]/a/text()')[0]
    category_id = book_info[5].xpath('td[2]/a/@href')[0].split('sok=')[1].split('&')[0]
    
    categories = book_info[5].xpath('td[2]/a/text()')
    category_ids = [ id.split('sok=')[1].split('&')[0].strip() for id in book_info[5].xpath('td[2]/a/@href') ]
    #print categories
    #if any([ item in NOT_CATEGORIES for item in categories_id ]):
        #print "found conflicting catergory"
    
    offset = 0
    score = 0
    if len(book_info) == 14:
        score = len(book_info[6].xpath("td/table/tr/td/img[@src='../images/star_filled2.gif']"))
    else:
        offset = 14 - len(book_info)
        
    language = book_info[10 - offset].xpath('td[2]/span/text()')[0]
    publisher = book_info[11 - offset].xpath('td[2]/span/text()')[0]
    date = book_info[12 - offset].xpath('td[2]/span/text()')[0]
    
    length = length.split(' ')[1::2]
    l = 0
    for t in length:
        l = 60 * l + int(t)
    length = l
    
    book_info = {
        'author': author.strip(),
        'title': title.strip(),
        'others': others,
        'isbn10': isbn10.strip(),
        'isbn13': isbn.to_isbn13(isbn10).strip(),
        'bookformat': bookformat.strip(),
        'length': length,
        'description': description.strip(),
        'category': category.strip(),
        'category_id': category_id.strip(),
        'category_ids': category_ids,
        'categories': categories,
        'language': language.strip(),
        'publisher': publisher.strip(),
        'date': date.strip(),
        'score': score
    }
    return elib_sync.sanitize_fields(book_info)
        
def check(db):
    #db = elib_sync.find_isbns(PATH)
    downloaded = []
    
    i = 0
    for isbn13, path in db.iteritems():
        isbn10 = isbn.to_isbn10(isbn13)
        i += 1
            
        checkedfile = elib_conf.PATH + '/' + path + '/.checked'
        if (os.path.exists(checkedfile)):
            continue
        
        book_info = get_book_info(isbn10)
        
        actual_length = calculate_length(elib_conf.PATH + '/' + path)
        ratio = float(actual_length) / float(book_info['length'])
        
        if any([ item in elib_conf.NOT_CATEGORIES for item in book_info['category_ids'] ]):
            print("[%d/%d] %s - %s: Ignoring category (%s)" % (i, len(db), book_info['author'], book_info['title'], ', '.join(book_info['categories'])))
            path = move_book_to_path(path, 'ignored/')
            db[book_info['isbn13']] = path
        elif book_info['author'] in elib_conf.NOT_AUTHORS:
            print("[%d/%d] %s - %s: Ignoring author" % (i, len(db), book_info['author'], book_info['title']))
            path = move_book_to_path(path, 'ignored/')
            db[book_info['isbn13']] = path
        elif (ratio < elib_conf.MIN_DOWNLOADED):
            #retval += 1
            print("[%d/%d] %s - %s: %d of %d = %f" % (i, len(db), book_info['author'], book_info['title'], actual_length, book_info['length'], ratio))
            #print "%s: %d of %d = %f" % (path, actual_length, book_info['length'], ratio)
            directory = elib_sync.download_book(book_info, db, True)
            db[book_info['isbn13']] = directory
            downloaded.append(book_info['isbn13'])
        else:
            set_id3(elib_conf.PATH + '/' + path, book_info)
            elib_sync.write_info(book_info, elib_conf.PATH + '/' + path)
            print("[%d/%d] %s - %s: %d of %d = %f" % (i, len(db), book_info['author'], book_info['title'], actual_length, book_info['length'], ratio))
            path = move_book(path, book_info)
            db[book_info['isbn13']] = path
            checkedfile = elib_conf.PATH + '/' + path + '/.checked'
            open(checkedfile, 'w').close()
        
        #print book_info
        time.sleep(1)
            #break
    return (db, downloaded)
            
def move_book(path, book):
    newpath = book['category'].encode('utf-8') + '/' + book['author'].encode('utf-8')
    newfolder = book['date'].encode('utf-8') + ' - ' + book['title'].encode('utf-8')
    
    if newpath + '/' + newfolder == path:
        return path
        
    print(' %s -> %s' % (path, newpath + '/' + newfolder))
    os.renames(elib_conf.PATH + '/' + path, elib_conf.PATH + '/' + newpath + '/' + newfolder)
    return newpath + '/' + newfolder
    
            
def move_book_to_path(old_path, directory):
    path = old_path
    if not old_path.startswith(directory):
        path = directory + path
        print(" %s -> %s" % (old_path, path))
        os.renames(elib_conf.PATH + '/' + old_path, elib_conf.PATH + '/' + path)
    filelist = [ f for f in os.listdir(elib_conf.PATH + '/' + path) if f.endswith(".mp3") ]
    for f in filelist:
        os.remove(elib_conf.PATH + '/' + path + '/' + f)
    open(elib_conf.PATH + '/' + path + '/.checked', 'w').close()
    return path
    

        
def set_id3(path, data):
    filelist = [ f for f in os.listdir(path) if f.endswith(".mp3") ]
    filelist.sort()
    i = 0
    for f in filelist:
        i = i+1
        
        audio = MP3(path + '/' + f)
        
        audio["TALB"] = TALB(encoding=3, text=[data['title']])
        audio["TPE2"] = TPE2(encoding=3, text=[data['author']])
        audio["TPE1"] = TPE1(encoding=3, text=[data['author']])
        audio["COMM"] = COMM(encoding=3, text=[data['description']])
        audio["TCOM"] = TCOM(encoding=3, text=data['others'])
        audio["TIT1"] = TIT1(encoding=3, text=[data['bookformat']])
        audio["TPOS"] = TPOS(encoding=3, text='1/1')
        audio["TCON"] = TCON(encoding=3, text=[data['category']])
        audio["TLAN"] = TLAN(encoding=3, text=[data['language']])
        audio["TIT2"] = TIT2(encoding=3, text=[data['title'] + ', part %d' % i])
        audio["TRCK"] = TRCK(encoding=3, text='%d/%d' % (i, len(filelist)))
        audio["TYER"] = TYER(encoding=3, text=[data['date'][:4]])
        audio["TDAT"] = TDAT(encoding=3, text=['01' + data['date'][4:]])
        audio["TPUB"] = TPUB(encoding=3, text=[data['publisher']])
        audio["TKEY"] = TKEY(encoding=3, text=[data['isbn13']])
        audio["POPM"] = POPM(encoding=3, rating=data['score'], count=0)
        
        audio.save()
        
    
        
def calculate_length(path):
    filelist = [ f for f in os.listdir(path) if f.endswith(".mp3") ]
    totalTime = 0
    for f in filelist:
        mf = mad.MadFile(path + '/' + f)
        track_length_in_milliseconds = mf.total_time()
        totalTime += track_length_in_milliseconds
    return totalTime / 60000
    
    
if __name__=='__main__':
    db = elib_sync.find_isbns(elib_conf.PATH)
    sys.exit(check(db))
    
