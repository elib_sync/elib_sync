#!/usr/bin/python
# -*- coding: utf-8 -*-

LANG = [
    #'TR', # Turkiska
    #'DE', # Tyska
    #'CS', # Tjeckiska
    'SE', # Svenska
    #'IT', # Italienska
    #'SO', # Somaliska
    #'RO', # Rumänska
    #'DK', # Danska
    #'EN', # Engelska
    #'AR', # Arabiska
    #'TH', # Thailändska
    #'FR', # Franska
    #'KU', # Kurdiska
    #'RU', # Ryska
    #'FA', # Farsi
    #'ES', # Spanska
    #'NO', # Norska
    #'FI', # Finska
]
LIBRARY = '38'
CATEGORIES = [
    #'1',   # Skönlitteratur
    #'101', # Aforismer & citat
    #'103', # Deckare & spänning
    #'104', # Dramatik
    #'105', # Fantasy & SF
    #'111', # Humor
    #'112', # Litterära essäer
    #'106', # Lyrik
    #'107', # Noveller
    #'102', # Romaner
    #'110', # Tecknade serier

    #'2',   # Barn & ungdom
    #'208', # 0-3 år
    #'204', # 12-15 år
    #'207', # 3-6 år
    #'206', # 6-9 år
    #'205', # 9-12 år
    #'201', # Barn & Ungdom
    #'202', # Barnböcker med tecken för hörande barn
    #'209', # Bilderböcker
    #'203', # Unga vuxna

    #'5',   # Lättläst
    #'501', # Lättläst

    #'3',   # Facklitteratur
    #'321', # Biologi
    #'302', # Ekonomi  & marknadsföring
    #'316', # EMU och EU
    #'311', # Filosofi
    #'317', # Fiske, jakt & skytte
    #'305', # Historia
    #'320', # Historiska faksimil
    #'327', # Hobby, spel & lekar
    #'306', # Humaniora
    #'304', # Hus & hem
    #'318', # Idrott & friluftsaktiviteter
    #'314', # IT & teknik
    #'307', # Juridik
    #'319', # Konst, musik, teater, film
    #'303', # Kulturhistoria
    #'328', # Litteraturvetenskap
    #'323', # Matematik & statistik
    #'326', # Matlagning, mat & dryck
    #'301', # Media & kommunikation
    #'308', # Medicin & hälsa
    #'322', # Memoarer & Biografier
    #'324', # Naturvetenskap
    #'309', # Pedagogik
    #'310', # Psykologi
    #'325', # Religion
    #'315', # Resor & geografi
    #'312', # Samhälle & politik
    #'313', # Språk & ordböcker
]

NOT_CATEGORIES = [
    #'1',   # Skönlitteratur
    #'101', # Aforismer & citat
    #'103', # Deckare & spänning
    #'104', # Dramatik
    #'105', # Fantasy & SF
    #'111', # Humor
    #'112', # Litterära essäer
    #'106', # Lyrik
    #'107', # Noveller
    #'102', # Romaner
    #'110', # Tecknade serier

    #'2',   # Barn & ungdom
    #'208', # 0-3 år
    #'204', # 12-15 år
    #'207', # 3-6 år
    #'206', # 6-9 år
    #'205', # 9-12 år
    #'201', # Barn & Ungdom
    #'202', # Barnböcker med tecken för hörande barn
    #'209', # Bilderböcker
    #'203', # Unga vuxna

    #'5',   # Lättläst
    #'501', # Lättläst

    #'3',   # Facklitteratur
    #'321', # Biologi
    #'302', # Ekonomi  & marknadsföring
    #'316', # EMU och EU
    #'311', # Filosofi
    #'317', # Fiske, jakt & skytte
    #'305', # Historia
    #'320', # Historiska faksimil
    #'327', # Hobby, spel & lekar
    #'306', # Humaniora
    #'304', # Hus & hem
    #'318', # Idrott & friluftsaktiviteter
    #'314', # IT & teknik
    #'307', # Juridik
    #'319', # Konst, musik, teater, film
    #'303', # Kulturhistoria
    #'328', # Litteraturvetenskap
    #'323', # Matematik & statistik
    #'326', # Matlagning, mat & dryck
    #'301', # Media & kommunikation
    #'308', # Medicin & hälsa
    #'322', # Memoarer & Biografier
    #'324', # Naturvetenskap
    #'309', # Pedagogik
    #'310', # Psykologi
    #'325', # Religion
    #'315', # Resor & geografi
    #'312', # Samhälle & politik
    #'313', # Språk & ordböcker
]

NOT_AUTHORS = [
    #'Jan Guillou',
    #'Mark Levengood',
]
PATH = '/mnt/elib'
MIN_DOWNLOADED = 0.96
