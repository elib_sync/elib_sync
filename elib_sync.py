#!/usr/bin/python
# -*- coding: utf-8 -*-

from lxml import etree

import urllib2
import sys, os
import time, string, re
import isbn
import elib_get
import elib_conf
import json

URL = 'http://www.elib.se/library/search.asp?lang=%(lang)s&BCAT=%(category)s&typ71=71&lib=%(library)s&secondrun=YES&iPage=%(page)s'


def get_num_pages(html):
    tree = etree.HTML(html)
    
    pages_text = tree.xpath('/html/body/table/tr/td/table/tr/td/table/tr/td/table/tr/td/table/tr/td/span/b/text()')

    num_pages = pages_text[0].split(' ')[4][:-1]
    
    return int(num_pages)

def get_entries(html):
    tree = etree.HTML(html)
    
    books = []
    
    for t in range(4, 6):
        entries = tree.xpath('/html/body/table/tr/td/table/tr/td/table/tr[%d]/td/table/tr' % t)
        
        for e in entries[2:-1:2]:
            title = ' '.join(e.xpath('td[3]/b/a/text()')[0].split(' ')[1:])
            title.replace(u' / Lättläst', '')
            author = e.xpath('td[3]/span[1]/text()')[0][3:]
            date = e.xpath('td[3]/span[2]/text()')[0].split(' ')
            isbn13 = date[2][:-1]
            date = date[0]
            publisher, language = e.xpath('td[3]/span[3]/text()')[0].split(', ')
            book = sanitize_fields({
                    'title': title.strip(),
                    'author': author.strip(),
                    'date': date.strip(),
                    'isbn13': isbn13.strip(),
                    'isbn10': isbn.to_isbn10(isbn13).strip(),
                    'publisher': publisher.strip(),
                    'language': language.strip()
                    })
            books.append(book)
        
    return books
        
def write_info(book, directory):
    #if os.path.exists(directory):
    info = open(directory + '/.ISBN:' + book['isbn13'], 'w')
    json.dump(book, info)
    info.write('\n')
    info.close()

def find_isbns(path):
    db = {}
    for root, dirs, files in os.walk(path):
        isbns = [ f for f in files if f.startswith(".ISBN:") ]
        for isbn13 in isbns:
            isbn13 = isbn13[6:]
            if isbn.valid(isbn13):
                db[isbn.to_isbn13(isbn13)] = root[len(path)+1:]
    #print db
    return db
            
def sanitize_fields(book):
    if 'title' in book:
        book['title'] = book['title'].split('/')[0].strip()
    if 'category' in book:
        book['category'] = book['category'].replace('/', '_').strip()
    if 'author' in book:
        book['author'] = book['author'].replace('/', '_').strip()
    if 'date' in book:
        book['date'] = book['date'].replace('/', '_').strip()
    return book
        
def download_book(b, db, force = False, download = True):
    directory = b['isbn13']
    
    if b['isbn13'] in db:
        #print "found isbn in db", b['isbn13']
        directory = db[b['isbn13']]
    #else:
        #print db
        #print "did not find isbn in db", b['isbn13']
        
    filename = b['author'] + ' - ' + b['title']
    
    filename = filename.replace('/', '_').replace('\n', '_').replace('\0', '_').encode('utf-8')
    
    #filename = directory + '/' + filename
    
    #foundfile = find('.ISBN:' + b['isbn13'], PATH)
    if not force and b['isbn13'] in db:
        pass
        #print 'Skipping existing book:', directory
    else:
        if not os.path.exists(elib_conf.PATH + '/' + directory):
            os.mkdir(elib_conf.PATH + '/' + directory)
        else:
            #print 'Cleaning directory of unfinished book'
            filelist = [ f for f in os.listdir(elib_conf.PATH + '/' + directory) if f.endswith(".mp3") ]
            for f in filelist:
                os.remove(elib_conf.PATH + '/' + directory + '/' + f)
        write_info(b, elib_conf.PATH + '/' + directory)
        #print 'Fetching book', filename
        if download:
            elib_get.download_book(b['isbn10'].encode('utf-8'), elib_conf.PATH + '/' + directory + '/' + filename)
    return directory
        
def sync(db):
    for lang in elib_conf.LANG:
        for category in elib_conf.CATEGORIES:
            #print 'Category %s, page 1' % category
            resp = urllib2.urlopen(URL % {'lang': lang, 'library': elib_conf.LIBRARY, 'category': category, 'page': 1})
            html = resp.read()
            print (URL % {'lang': lang, 'library': elib_conf.LIBRARY, 'category': category, 'page': 1})
            html = html.decode('iso-8859-15')
            num_pages = get_num_pages(html)
            sys.stdout.write('%s Category %s, %d pages: 1' % (lang, category, num_pages))
            sys.stdout.flush()

            books = get_entries(html)

            for page in range(2, num_pages + 1):
                time.sleep(1)
                #print 'Category %s, page %d of %d' % (category, page, num_pages)
                sys.stdout.write(', %d' % page)
                sys.stdout.flush()
                resp = urllib2.urlopen(URL % {'lang': lang, 'library': elib_conf.LIBRARY, 'category': category, 'page': page})
                html = resp.read()
                html = html.decode('iso-8859-15')
                books += get_entries(html)
            print('')
                
            for b in books:
                b['category_id'] = category
                directory = download_book(b, db, False, False)
                if not b['isbn13'] in db:
                    print('Adding %s: %s - %s' % (b['isbn13'], b['author'], b['title']))
                    db[b['isbn13']] = directory
    return db
    
if __name__=='__main__':
    db = find_isbns(elib_conf.PATH)
    sync(db)
    
