#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, os
import elib_sync
import elib_check
import elib_conf

#PATH = '/var/disks/disk2/elib'

if __name__=='__main__':
    db = elib_sync.find_isbns(elib_conf.PATH)
    db = elib_sync.sync(db)
    downloaded = []
    for t in range(0, 3):
        db, downloaded = elib_check.check(db)
        if len(downloaded) == 0:
            break
#        else:
#            print downloaded
    for d in downloaded:
        elib_check.move_book_to_path(db[d], 'broken/')
